from django import forms
from datetime import date, datetime

from nocaptcha_recaptcha.fields import NoReCaptchaField

from .models import Voter

class CreateElectionForm(forms.Form):
	name = forms.CharField(label='Nazwa wyborów', max_length=255)
	start_date = forms.DateField(label='Początek wyborów')
	end_date = forms.DateField(label='Koniec wyborów')
	start_time = forms.TimeField(label='Godzina startu wyborów')
	end_time = forms.TimeField(label='Godzina zakończenia wyborów')
	max_votes_from_one = forms.IntegerField(label='Maksymalna liczba głosów oddanych przez jednego uczestnika', min_value=1)
	description = forms.CharField(label='Opis', required=False)
	email = forms.EmailField(label='Email twórcy wyborów')
	captcha = NoReCaptchaField()
	hidden = forms.BooleanField(required=False)
		
	def clean(self):
		cleaned_data = super(CreateElectionForm, self).clean()
		#data = cleaned_data['end_date']
		#startd = cleaned_data['start_date']
		startd = datetime.combine(cleaned_data['start_date'], cleaned_data['start_time'])
		data = datetime.combine(cleaned_data['end_date'], cleaned_data['end_time'])
		
		if startd <= datetime.now():
			self.add_error('start_date', 'Początek wyborów musi być przyszłą datą.')
        
		if data <= datetime.now() or data <= startd:
			self.add_error('end_date', 'Podaj poprawną datę końca wyborów.')
			
		return cleaned_data

	  
class ManageElectionForm(forms.Form):
	name = forms.CharField(label='Nazwa wyborów', max_length=255)
	start_date = forms.DateField(label='Początek wyborów')
	end_date = forms.DateField(label='Koniec wyborów')
	start_time = forms.TimeField(label='Godzina startu wyborów')
	end_time = forms.TimeField(label='Godzina zakończenia wyborów')
	max_votes_from_one = forms.IntegerField(label='Maksymalna liczba głosów oddanych przez jednego uczestnika', min_value=1)
	description = forms.CharField(label='Opis', required=False)

	def clean(self):
		cleaned_data = super(ManageElectionForm, self).clean()
		#data = cleaned_data['end_date']
		#startd = cleaned_data['start_date']
		startd = datetime.combine(cleaned_data['start_date'], cleaned_data['start_time'])
		data = datetime.combine(cleaned_data['end_date'], cleaned_data['end_time'])
        
		if startd <= datetime.now():
			self.add_error('start_date', 'Początek wyborów musi być przyszłą datą.')
        
		if data <= datetime.now() or data <= startd:
			self.add_error('end_date', 'Podaj poprawną datę końca wyborów.')
			
		return cleaned_data

 
class AddCandidateForm(forms.Form):
	firstname = forms.CharField(label='Imię', max_length=64)
	lastname= forms.CharField(label='Nazwisko', max_length=64)
	description = forms.CharField(label='Opis', required=False)
	photo = forms.ImageField(label='Zdjęcie', required=False)
	
	
class AddVoterForm(forms.Form):
	firstname = forms.CharField(label='Imię', max_length=64)
	lastname= forms.CharField(label='Nazwisko', max_length=64)
	pesel = forms.CharField(label='PESEL', required=True)
	email = forms.EmailField(label='Email')
	
	def clean(self):
		cleaned_data = super(AddVoterForm, self).clean()
		pesel = cleaned_data['pesel']
		
		if not Voter.checkPesel(pesel):
			self.add_error('pesel', 'Musisz podać prawidłowy numer PESEL.')
			
		return cleaned_data

