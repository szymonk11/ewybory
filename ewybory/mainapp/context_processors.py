from .models import Election


def add_variable_to_context(request):
    return {
        'pre_elections': Election.getPreElections()
    }