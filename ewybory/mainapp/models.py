from django.db import models
from django.http import HttpResponse
from datetime import date
import uuid
import re
import json

class Election(models.Model):
	name = models.CharField(max_length=255)
	start_date = models.DateTimeField()
	end_date = models.DateTimeField(blank=True,null=True)
	max_votes_from_one = models.IntegerField(default=1)
	verify_email = models.IntegerField(default=0)
	description = models.TextField(blank=True,null=True)
	access_code = models.CharField(max_length=255,null=True)
	admin_code = models.CharField(max_length=255,null=True)
	admin_email = models.EmailField(blank=True,null=True)
	hidden = models.BooleanField(default=False)
	
	def checkVoter(self, gpesel):
		voters = Voter.objects.filter(election=self).all()
		if len(voters) > 0:
			can = Voter.objects.filter(election=self, pesel=gpesel).all()
			if len(can) <= 0:
				return False
					
		votes = Vote.objects.filter(election=self.id, pesel=gpesel).count()
		if votes == 0:
			return True
		else:
			return False
	
	def vote(self, gpesel, candidate):
		vote = Vote()
		vote.election = self
		vote.pesel = gpesel
		vote.save()
		candidate.votes += 1
		return candidate.save()
		
	def generateAccessCode(self):
		return uuid.uuid4().hex[:6].upper()
		
	def generateAdminCode(self):
		return uuid.uuid4().hex[:12].upper()
	
	@classmethod
	def getPreElections(cls):
		return cls.objects.filter(hidden=False, end_date__lte = date.today()).all()
	
	def __str__(self):
		return self.name
		
	class Meta:
		verbose_name = 'Wybory'
		verbose_name_plural = 'Wybory'
			
	
class Candidate(models.Model):
	election = models.ForeignKey('Election', on_delete=models.CASCADE)
	firstname = models.CharField(max_length=64)
	lastname = models.CharField(max_length=64)
	description = models.TextField(blank=True, null=True)
	photo = models.ImageField(default="p_male.png", blank=True, null=True)
	votes = models.IntegerField(default=0)
	
	def getPercent(self, cnt):
		return self.votes/cnt*100
	
	def __str__(self):
		return "%s %s" % (self.firstname, self.lastname)
		
	class Meta:
		verbose_name = 'Kandydat'
		verbose_name_plural = 'Kandydaci'
	
class Voter(models.Model):
	election = models.ForeignKey('Election', on_delete=models.CASCADE,)
	pesel = models.CharField(max_length=11)
	firstname = models.CharField(max_length=64)
	lastname = models.CharField(max_length=64)
	email = models.EmailField()
	
	@staticmethod
	def checkPesel(pesel):
		result = re.search(r'^[0-9]{11}$', pesel)
		if not result:
			return False
		
		sum = 1 * int(pesel[0]) + 3*int(pesel[1]) + 7*int(pesel[2]) + 9*int(pesel[3]) + 1*int(pesel[4]) + 3*int(pesel[5]) + 7*int(pesel[6]) + 9*int(pesel[7]) + 1*int(pesel[8]) + 3*int(pesel[9])
		sum = (10 - (sum % 10) ) % 10
		
		if sum != int(pesel[10]):
			return False
		
		month = 10 * int(pesel[2]) + int(pesel[3])
		if month > 80 and month < 93:
			month -= 80
		elif month > 20 and month < 33:
			month -= 20
		elif month > 40 and month < 53:
			month -= 40
		elif month > 60 and month < 73:
			month -= 60
		
		if month < 1 or month > 12:
			return False
			
		day = 10 * int(pesel[4]) + int(pesel[5])
		if day < 1 or day > 31:
			return False
		
		return pesel
	
	def __str__(self):
		return "%s %s" % (self.firstname, self.lastname)
		
	class Meta:
		verbose_name = 'Uprawniony'
		verbose_name_plural = 'Uprawnieni'
	
class Vote(models.Model):
	election = models.ForeignKey('Election', on_delete=models.CASCADE)
	pesel = models.CharField(max_length=11)
	date = models.DateTimeField(auto_now=True)
	
class Report(models.Model):
	election = models.ForeignKey('Election', on_delete=models.CASCADE)
	created = models.DateTimeField(auto_now=True)
	pdf = models.FileField(upload_to='pdfs/')
