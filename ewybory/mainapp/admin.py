from django.contrib import admin
from django.contrib.contenttypes.admin import GenericTabularInline
from datetime import date

# Register your models here.
from .models import Election, Candidate, Voter, Vote, Report

class CandidateAdmin(admin.ModelAdmin):
	list_display = ('firstname', 'lastname', 'photo', 'election', 'view_description', 'votes', )
	fields = ('election', ('firstname', 'lastname'), 'description', 'photo', 'votes', )
	#readonly_fields = ('votes', )
	search_fields  = ('firstname', 'lastname', 'election', )
	ordering = ['-election']
	#actions = ['clear_votes']
	
	def view_description(self, obj):
		return (obj.description[:50] + '...') if len(obj.description) > 50 else obj.description
		
	def clear_votes(self, request, queryset):
		queryset.update(vote=0)
	clear_votes.short_description = "Wyczyść głosy"


class CandidateInline(admin.TabularInline):
	model = Candidate
	#readonly_fields = ('votes', )


class VoterAdmin(admin.ModelAdmin):
	list_display = ('firstname', 'lastname', 'email', 'election', )
	fields = ('election', ('firstname', 'lastname'), 'email', 'pesel', )
	#readonly_fields = ('email', 'pesel', )
	search_fields  = ('firstname', 'lastname', 'email', 'pesel', 'election', )
	ordering = ['-election', '-firstname', '-lastname']


class VoterInline(admin.TabularInline):
	model = Voter
	

class ElectionAdmin(admin.ModelAdmin):
	model = Election
	list_display = ('id', 'name', 'start_date', 'end_date', 'max_votes_from_one', 'view_description', 'admin_email', 'hidden')
	fields = ('id', 'name', ('start_date', 'end_date'), 'max_votes_from_one', 'description', 'access_code', 'admin_code', 'admin_email', 'hidden')
	readonly_fields = ('id', )
	search_fields  = ('id', 'name', 'start_date', 'end_date', 'admin_email', 'hidden', )
	
	ordering = ['id']
	
	inlines = [
		CandidateInline,
		VoterInline,
	]
	
	def view_description(self, obj):
		return (obj.description[:75] + '...') if len(obj.description) > 75 else obj.description
		
	


admin.site.register(Election, ElectionAdmin)
admin.site.register(Candidate, CandidateAdmin)
admin.site.register(Voter, VoterAdmin)
