from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from . import views

urlpatterns = [
	path('', views.home , name='home'),
	path('create_election', views.create_election, name='create_election'),
	path('manage_election/<int:id>/delete_candidate/<int:candidate>', views.manage_election_delete_candidate, name='manage_election_delete_candidate'),
	path('manage_election/<int:id>/delete_voter/<int:voter>', views.manage_election_delete_voter, name='manage_election_delete_voter'),
	path('manage_election/<int:id>', views.manage_election, name='manage_election'),
	path('election/<int:id>/results', views.election_results, name='election_results'),
	path('election/<int:id>', views.election, name='election'),
	path('vote/<int:id>', views.vote, name='vote'),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)