from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponse
from django.urls import reverse
from urllib.parse import urlencode
from django.contrib import messages
from django.core.mail import send_mail
from datetime import date, datetime
import pytz

from .models import Election, Candidate, Voter, Vote

from .forms import CreateElectionForm, AddCandidateForm, ManageElectionForm, AddVoterForm

def home(request):
	
	act_elections = Election.objects.filter(hidden=False, start_date__lte = datetime.now(), end_date__gte = datetime.now()).all()
	fol_elections = Election.objects.filter(hidden=False, start_date__gte = datetime.now()).all()
	pre_elections = Election.objects.filter(hidden=False, end_date__lte = datetime.now()).all()

	return render(request, 'home.html', {'act_elections': act_elections, 'fol_elections': fol_elections, 'pre_elections': pre_elections})
	
	
def election(request, id):
	election = get_object_or_404(Election, pk=id)
	candidates = Candidate.objects.filter(election=id).all()
	
	now = datetime.now()
	now = pytz.utc.localize(now)
	open = True
	if election.start_date > now:
		open = False
	
	access_code = request.session.get('access_code', False)
	if access_code and access_code == election.access_code:  
		return render(request, 'election.html', {'election': election, 'candidates': candidates, 'open': open})
		
	else:
		if request.method == 'POST':
			if 'enter' in request.POST:
				code = request.POST.get('code', '')
				
				if code == election.access_code:
					request.session['access_code'] = election.access_code
					#messages.success(request, 'Logowanie pomyślne.')
					return render(request, 'election.html', {'election': election, 'candidates': candidates, 'open': open})
				else:
					messages.error(request, 'Podany kod jest nieprawidłowy.')
		
		return render(request, 'enter_code.html')


def election_results(request, id):
	election = get_object_or_404(Election, pk=id)
	
	now = datetime.now()
	now = pytz.utc.localize(now)
	
	if election.end_date > now:
		messages.error(request, 'Te wybory jeszcze trwają.')
		return redirect('home')
	
	num_votes = Vote.objects.filter(election=election).count()
	candidates = Candidate.objects.filter(election=id).all()
	
	results = []
	max = 0
	
	for c in candidates:
		results.append({
			'firstname': c.firstname,
			'lastname': c.lastname,
			'percent': 0 if num_votes == 0 else round(c.votes / num_votes * 100, 2)
		})
		if num_votes != 0 and round(c.votes / num_votes * 100, 2) > max:
			max = round(c.votes / num_votes * 100, 2)
	
	results.sort(key=lambda x: x['percent'], reverse=True)
	
	return render(request, 'election_results.html', {'election': election, 'max': max, 'results': results, 'num_votes': num_votes})
	

def vote(request, id):
	election = get_object_or_404(Election, pk=id)
	candidates = Candidate.objects.filter(election=id).all()
	
	now = datetime.now()
	now = pytz.utc.localize(now)
	
	if election.end_date < now:
		messages.error(request, 'Wybory zostały już zakończone.')
		return redirect('home')
	
	if election.start_date > now:
		messages.error(request, 'Wybrane wybory jeszcze się nie zaczęły.')
		return redirect('home')
	
	if( 'submit' in request.POST ):
		pesel = request.POST.get('pesel', False)
		pesel = Voter.checkPesel(pesel)
		if pesel == False:
			messages.error(request, 'Musisz podać prawidłowy numer PESEL.')
		else:
			if election.checkVoter(pesel):
			
				check_candidates = []
				candidates_ids = request.POST.getlist('candidate')
				for cid in candidates_ids:
					try:
						candidate = Candidate.objects.get(pk=cid)
					except(KeyError, Candidate.DoesNotExist):
						messages.error(request, 'Wystąpił nieoczekiwany błąd.')
					else:
						check_candidates.append(candidate)

				if len(check_candidates) < 1 :
					messages.error(request, 'Musisz wybrać jakiegoś kandydata.')
				elif len(check_candidates) > election.max_votes_from_one:
					messages.error(request, 'Możesz wybrać maksymalnie %d kandydatów.' % (election.max_votes_from_one))
				else:
					for candidate in check_candidates:
						election.vote(pesel, candidate)
					
					messages.success(request, 'Twój głos został zapisany.')
					return redirect('home')
					
			else:
				messages.error(request, 'Nie możesz oddać głosu.')
				return redirect('home')
			
	
	return render(request, 'vote.html', {'election': election, 'candidates': candidates})
	
def manage_election(request, id):
	election = get_object_or_404(Election, pk=id)
	candidates = Candidate.objects.filter(election=id).all()
	voters = Voter.objects.filter(election=id).all()
	
	admin_code = request.session.get('admin_code', False)
	
	form = ManageElectionForm()
	add_candidate_form = AddCandidateForm()
	add_voter_form = AddVoterForm()
	
	if admin_code and admin_code == election.admin_code:
	
		if request.method == 'POST':
			if request.POST.get('submit') == 'add_candidate':
				add_candidate_form = AddCandidateForm(request.POST)
				if add_candidate_form.is_valid():
					candidate = Candidate()
					candidate.election = election
					candidate.firstname = add_candidate_form.cleaned_data['firstname']
					candidate.lastname = add_candidate_form.cleaned_data['lastname']
					candidate.description = add_candidate_form.cleaned_data['description']
					candidate.photo = request.FILES.get('photo', 'p_male.png')
					candidate.save()
					messages.success(request, 'Nowy kandydat został dodany.')
					return redirect('manage_election', id=id)
			
			elif request.POST.get('submit') == 'add_voter':
				add_voter_form = AddVoterForm(request.POST)
				if add_voter_form.is_valid():
					exists = Voter.objects.filter(election=id, pesel=add_voter_form.cleaned_data['pesel']).all()
					if len(exists) > 0:
						messages.error(request, 'Osoba o podanym numerze PESEL została już uprawniona do głosowania.')
					else: 
						nvoter = Voter()
						nvoter.election = election
						nvoter.pesel = add_voter_form.cleaned_data['pesel']
						nvoter.firstname = add_voter_form.cleaned_data['firstname']
						nvoter.lastname = add_voter_form.cleaned_data['lastname']
						nvoter.email = add_voter_form.cleaned_data['email']
						nvoter.save()
						messages.success(request, 'Nowy uprawniony został dodany.')
						
					return redirect('manage_election', id=id)
				
			else:
				form = ManageElectionForm(request.POST)
				if form.is_valid():
					election.name = form.cleaned_data['name']
					election.start_date = datetime.combine(form.cleaned_data['start_date'], form.cleaned_data['start_time'])
					election.end_date = datetime.combine(form.cleaned_data['end_date'], form.cleaned_data['end_time'])
					election.max_votes_from_one = form.cleaned_data['max_votes_from_one']
					election.description = form.cleaned_data['description']
					election.save()
					messages.success(request, 'Zmiany zostały pomyślnie zapisane.')
					return redirect('manage_election', id=id)
			
	
		return render(request, 'manage_election.html', {'election': election, 'candidates': candidates, 'voters': voters, 'add_candidate_form': add_candidate_form, 'add_voter_form': add_voter_form, 'form': form})
	else:
		if request.method == 'POST':
			if 'enter' in request.POST:
				code = request.POST.get('code', '')
				#return HttpResponse(code)
				if code == election.admin_code:
					request.session['admin_code'] = election.admin_code
					messages.success(request, 'Logowanie pomyślne.')
					return render(request, 'manage_election.html', {'election': election, 'candidates': candidates, 'voters': voters, 'add_candidate_form': add_candidate_form, 'add_voter_form': add_voter_form, 'form': form})
				else:
					messages.error(request, 'Podany kod jest nieprawidłowy.')
					#return render(request, 'enter_code.html')
			#else:
				#return render(request, 'enter_code.html')
		#else:
		return render(request, 'enter_code.html')

def manage_election_delete_candidate(request, id, candidate):
	election = get_object_or_404(Election, pk=id)
	cand = get_object_or_404(Candidate, pk=candidate)
	
	admin_code = request.session.get('admin_code', False)
	
	if admin_code and admin_code == election.admin_code:
		cand.delete()
		messages.success(request, 'Kandydat został usunięty.')
		
	return redirect('manage_election', id=id)

def manage_election_delete_voter(request, id, voter):
	election = get_object_or_404(Election, pk=id)
	vot = get_object_or_404(Voter, pk=voter)
	
	admin_code = request.session.get('admin_code', False)
	
	if admin_code and admin_code == election.admin_code:
		vot.delete()
		messages.success(request, 'Osoba uprawniona do głosowania została usunięta.')
		
	return redirect('manage_election', id=id)
	
def create_election(request):
	if request.method == 'POST':
		form = CreateElectionForm(request.POST)
		if form.is_valid():
			election = Election()
			election.name = form.cleaned_data['name']
			election.start_date = datetime.combine(form.cleaned_data['start_date'], form.cleaned_data['start_time'])
			election.end_date = datetime.combine(form.cleaned_data['end_date'], form.cleaned_data['end_time'])
			election.max_votes_from_one = form.cleaned_data['max_votes_from_one']
			election.description = form.cleaned_data['description']
			election.access_code = election.generateAccessCode()
			election.admin_code = election.generateAdminCode()
			election.admin_email = form.cleaned_data['email']
			election.hidden = form.cleaned_data['hidden']
			election.save()
			
			url_to_manage = request.build_absolute_uri(reverse('manage_election', kwargs={'id': election.id}))
			
			#send email here
			sended = send_mail(
				"Informacje dt. wyborów - %s" % election.name,
				"Link do zarządzania Twoimi wyborami: %s\r\n\nKod dostępu dla twórcy: %s\r\n\nKod dostępu dla wyborców: %s" % (url_to_manage, election.admin_code, election.access_code),
				"ewybory@szymonk.pl",
				[election.admin_email],
				fail_silently=True
			)
			if sended == 0:
				messages.error(request, 'Wystąpił nieoczekiwany błąd podczas wysyłania maila.')
			  
			messages.success(request, 'Wybory zostały pomyślnie stworzone. Na Twój adres e-mail wysłaliśmy szczegółowe informacje dotyczące zarządzania wyborami.')
			request.session['admin_code'] = election.admin_code
			return redirect('manage_election', id=election.id)
	
	else:
		form = CreateElectionForm()
	
	return render(request, 'create_election.html', {'form': form})