Do poprawnego działania należy wcześniej zainstalować:
- Python 3.8.0
- Django ^3.0.4
(https://docs.djangoproject.com/pl/3.0/intro/install/)

- reCaptcha
pip install django-nocaptcha-recaptcha
(https://github.com/ImaginaryLandscape/django-nocaptcha-recaptcha)

- Pillow
pip install Pillow

Uruchomienie serwera
$ cd wybory
$ python manage.py runserver
